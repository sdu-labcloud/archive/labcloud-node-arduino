#include "ConfigServer.hpp"
#include "LabCloudNode.h"

const int SERIAL_BAUD = 115200;
const String VERSION = "0.1.0";

// // Get pointer to serial output.
// HardwareSerial *console = &Serial;

LabCloudNode node;

// Create new server to handle configuration.
ConfigServer configServer(node.console);

void setup()
{
  node.init();
  node.print("\r");
  node.print("Version: ");
  node.print(VERSION + "\r");
}

void loop()
{
  if (!configServer.isWiFiConnected())
  {
    configServer.start();
    while (!configServer.isWiFiConnected())
    {
      configServer.run();
    };
    configServer.stop();
  }
}