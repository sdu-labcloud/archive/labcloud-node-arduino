# LabCloud Node Arduino

## Flashing node

This project uses PlatformIO to manage libraries and compile the code. It includes a file system on the ESP, so the data must be uploaded through PlatformIO or a similar tool.

If using PlatformIO, use the "Upload File System Image" function to upload the data.

## Configuring WiFi

The WiFi can be configured manually by a user or programmatically via a script. To configure it manually, follow the instructions below:

1. Connect the node to power.
2. Connect to the WiFi network with the name `LabCloud Node <LAST 4 CHARACTERS OF MAC>`.
3. In your browser, navigate to `192.168.4.1`.
4. Enter the network credentials and click `SAVE`.

To configure the WiFi programmatically, implement the instructions below:

1. Follow the steps **1** and **2** above.
2. Send an HTTP `PUT` request to `http://192.168.4.1/v1/wifi/config` with the following **JSON** body:
   ```json
   {
     "ssid": "MY WIFI NETWORK",
     "psk": "MY WIFI PASSWORD"
   }
   ```

## License

This project is licensed under the terms of the [MIT license](./LICENSE.md).
