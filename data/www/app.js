document.addEventListener('DOMContentLoaded', function() {
  function submitWifiForm(event) {
    // Prevent page reload.
    event.preventDefault();

    // Get SSID and PSK fields.
    const ssid = document.getElementById('wifi-form-ssid');
    const psk = document.getElementById('wifi-form-psk');

    // Prepare request to send configuration parameters.
    const payload = { ssid: ssid.value, psk: psk.value };
    const config = {
      // Define HTTP verb.
      method: 'PUT',
      // Transform JS object to valid JSON string.
      body: JSON.stringify(payload),
      // Be nice and set the content type header accordingly.
      headers: {
        'Content-Type': 'application/json'
      }
    };

    // Write configuration via REST API call.
    fetch('http://192.168.4.1/v1/wifi/config', config)
      .then(function(response) {
        return response.text();
      })
      .then(function() {
        // Clear form values to show success.
        ssid.value = '';
        psk.value = '';
      })
      .catch(function(err) {
        console.log(err);
      });
  }

  // Get button and attach event listener.
  const wifiFormButton = document.getElementById('wifi-form-submit');
  wifiFormButton.addEventListener('click', submitWifiForm);
});
