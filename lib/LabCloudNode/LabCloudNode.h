#include <Arduino.h>
#include <SoftwareSerial.h>

#include <Adafruit_NeoPixel.h>

#define NEOPIXEL_PIN 4
#define RELAY_PIN 12
#define LED_PIN 13
#define UART_RX_PIN 14
#define UART_TX_PIN 5

#define STATUS_READY 0
#define STATUS_APPROVED 1
#define STATUS_DENIED 2
#define STATUS_IDENTIFY 3
#define STATUS_MAINTENANCE 4
#define STATUS_OFF 5

class LabCloudNode
{
public:
    LabCloudNode();
    ~LabCloudNode(void);

    void init(void);

    void relayOn(void);
    void relayOff(void);

    void ledOn(void);
    void ledOff(void);

    void status(int statusMode);

    template <typename T>
    void print(T const &c)
    {
        this->console->print(c);
    };

    String nodeId;
    String sessionId;
    String permissionId;
    String userId;
    String scheduling;
    String workflow;
    String machineStatus;
    String token;

    bool authorized;
    bool identify;

    SoftwareSerial *console;

private:
    Adafruit_NeoPixel *indicator;
};