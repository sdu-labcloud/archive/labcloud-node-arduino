#include "LabCloudNode.h"

LabCloudNode::LabCloudNode(void)
{
    this->indicator = new Adafruit_NeoPixel(1, NEOPIXEL_PIN, NEO_RGB + NEO_KHZ800);
    this->console = new SoftwareSerial(UART_RX_PIN, UART_TX_PIN);
}

LabCloudNode::~LabCloudNode(void)
{
    delete this->indicator;
    delete this->console;
}

void LabCloudNode::init(void)
{
    // Set outputs
    pinMode(LED_PIN, OUTPUT);
    pinMode(RELAY_PIN, OUTPUT);

    // Set initial states
    this->relayOff();
    this->ledOn();

    // Start software serial
    this->console->begin(115200);
    this->print("Serial succesfully started!\n");

    // Start neopixel routine
    this->indicator->begin();
    this->print("Neopixel succesfully started!\n");
    this->status(STATUS_MAINTENANCE);
}

void LabCloudNode::relayOn(void)
{
    digitalWrite(RELAY_PIN, HIGH);
};

void LabCloudNode::relayOff(void)
{
    digitalWrite(RELAY_PIN, LOW);
};

void LabCloudNode::ledOn(void)
{
    digitalWrite(LED_PIN, HIGH);
};

void LabCloudNode::ledOff(void)
{
    digitalWrite(LED_PIN, LOW);
};

void LabCloudNode::status(int status)
{
    int time;
    // NOTICE
    // setPixelColor is in format GRB
    switch (status)
    {
    case STATUS_READY:
        this->indicator->setPixelColor(0, 255, 255, 255); //White
        this->indicator->show();
        break;

    case STATUS_APPROVED:
        this->indicator->setPixelColor(0, 255, 0, 0); //Green
        this->indicator->show();
        break;

    case STATUS_DENIED:
        this->indicator->setPixelColor(0, 0, 255, 0); //Red
        this->indicator->show();
        break;

    case STATUS_IDENTIFY:
        this->indicator->setPixelColor(0, 0, 255, 255); //Purple
        this->indicator->show();

        time = millis();
        while (millis() - time < 500)
        {
            yield();
        }
        this->status(STATUS_OFF);
        break;

    case STATUS_MAINTENANCE:
        this->indicator->setPixelColor(0, 200, 200, 0); //Orange
        this->indicator->show();
        break;

    case STATUS_OFF:
        this->indicator->setPixelColor(0, 0, 0, 0); //Black..
        this->indicator->show();
        break;

    default:
        this->indicator->setPixelColor(0, 0, 0, 0); //Black..
        this->indicator->show();
        break;
    }
};