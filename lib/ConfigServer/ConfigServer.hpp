#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>
#include <SoftwareSerial.h>

#include <ArduinoJson.h>
#include <FS.h>

const int HTTP_SERVER_PORT = 80;

class ConfigServer
{
public:
  ConfigServer(SoftwareSerial *c);
  void start(void);
  void run(void);
  void stop(void);
  bool isWiFiConnected(void);
  String getMacAddress(void);
  String getApMacAddress(void);

private:
  SoftwareSerial *console;
  ESP8266WebServer httpServer;
  String ssid;
  String psk;
  String apSsid;
  IPAddress apLocalIp;
  IPAddress apGatewayIp;
  IPAddress apSubnetMask;
  void serveFiles(void);
  void serveV1WifiConfig(void);
  String getContentType(String filename);
};