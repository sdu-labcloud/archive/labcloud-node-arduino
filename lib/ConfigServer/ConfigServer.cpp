#include "ConfigServer.hpp"

ConfigServer::ConfigServer(SoftwareSerial *c)
    : console(c), httpServer(HTTP_SERVER_PORT)
{
  // Configure general WiFi settings.
  WiFi.mode(WIFI_AP_STA);

  // Configure access point.
  this->apLocalIp = IPAddress(192, 168, 4, 1);
  this->apGatewayIp = IPAddress(192, 168, 4, 1);
  this->apSubnetMask = IPAddress(255, 255, 255, 0);
  this->apSsid = "labcloud-node-" + this->getApMacAddress().substring(12, 14) + this->getApMacAddress().substring(15, 17);

  // Configure HTTP server.
  this->httpServer.on("/v1/wifi/config", HTTP_PUT, std::bind(&ConfigServer::serveV1WifiConfig, this));
  this->httpServer.onNotFound(std::bind(&ConfigServer::serveFiles, this));
}

bool ConfigServer::isWiFiConnected(void)
{
  // Return the status of the WiFi connection.
  return WiFi.status() == WL_CONNECTED;
}

String ConfigServer::getMacAddress(void)
{
  // Return WiFi station MAC address.
  return String(WiFi.macAddress());
}

String ConfigServer::getApMacAddress(void)
{
  // Return WiFi AP MAC address.
  return String(WiFi.softAPmacAddress());
}

void ConfigServer::start(void)
{
  // Start file system.
  SPIFFS.begin();

  // Configure AP.
  this->console->print("Configuring soft-AP: ");
  this->console->println(
      WiFi.softAPConfig(this->apLocalIp, this->apGatewayIp, this->apSubnetMask)
          ? "Success"
          : "Failure");

  // Enable AP.
  this->console->print("Enabling soft-AP: ");
  this->console->println(WiFi.softAP(this->apSsid.c_str()) ? "Success"
                                                           : "Failure");

  // Get AP information.
  this->console->print("Soft-AP SSID: ");
  this->console->println(this->apSsid);
  this->console->print("Soft-AP IP: ");
  this->console->println(WiFi.softAPIP());

  // Start the HTTP server.
  this->httpServer.begin();
}

void ConfigServer::run(void)
{
  // Accept incoming HTTP requests.
  this->httpServer.handleClient();
}

void ConfigServer::stop(void)
{
  // Stop the HTTP server.
  this->httpServer.stop();

  // Disable AP.
  this->console->print("Disabling soft-AP: ");
  this->console->println(WiFi.softAPdisconnect(true) ? "Success" : "Failure");
}

String ConfigServer::getContentType(String filename)
{
  if (filename.endsWith(".html"))
    return "text/html";
  if (filename.endsWith(".css"))
    return "text/css";
  if (filename.endsWith(".js"))
    return "application/javascript";
  if (filename.endsWith(".ico"))
    return "image/x-icon";
  return "text/plain";
}

void ConfigServer::serveFiles(void)
{
  // Append webroot "www" folder to path.
  String uri = this->httpServer.uri();
  String path = "/www" + uri;

  // If a folder is requested, send the "index.html" file.
  if (path.endsWith("/"))
  {
    path += "index.html";
  }

  // Check if the file exists.
  if (SPIFFS.exists(path))
  {
    // Get the MIME type.
    String contentType = this->getContentType(path);
    // Open the file to get a file handle.
    File file = SPIFFS.open(path, "r");
    // Send file to client.
    this->httpServer.streamFile(file, contentType);
    // Close file.
    file.close();
    // Log response result to console.
    this->console->println("200 GET " + uri + " > " + path);
  }
  else
  {
    // Send error response.
    this->httpServer.send(404, "application/json", "{\"title\":\"Not Found\",\"status\":404}");
    // Log response result to the console.
    this->console->println("404 GET " + uri);
  }
}

void ConfigServer::serveV1WifiConfig(void)
{
  // Parse JSON document.
  StaticJsonDocument<100> doc;
  deserializeJson(doc, this->httpServer.arg("plain"));
  String payloadSsid = String(doc["ssid"].as<char *>());
  String payloadPsk = String(doc["psk"].as<char *>());

  // Check if SSID is set.
  if (payloadSsid.length())
  {
    // Configure WiFi.
    this->ssid = payloadSsid;
    this->psk = payloadPsk;
    WiFi.hostname(this->apSsid);
    WiFi.begin(this->ssid.c_str(), this->psk.c_str());

    // Send HTTP response.
    this->httpServer.send(200, "application/json",
                          this->httpServer.arg("plain"));
    this->console->println("200 PUT /v1/wifi/config");
  }
  else
  {
    // Send HTTP response.
    this->httpServer.send(400, "application/json",
                          "{\"error\":{\"title\":\"Bad Request\",\"status\":400,\"message\":\"The SSID is required and must not be empty.\"}}");
    this->console->println("400 PUT /v1/wifi/config");
  }
}